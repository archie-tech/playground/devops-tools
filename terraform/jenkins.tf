data "aws_ami" "ubuntu" {
  most_recent = true
  filter {
    name   = "name"
    values = ["ubuntu/images/hvm-ssd/ubuntu-focal-20.04-amd64-server-*"]
  }
  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
  owners = ["099720109477"] # Canonical
}

resource "aws_security_group" "jenkins" {
  name   = "Jenkins"
  vpc_id = aws_vpc.main.id
  # Allow SSH inbound
  ingress {
    description = "Allow SSH from anyware"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # Allow inbound access to Jenkins console
  ingress {
    description = "Allow access to Jenkins console from spesific IP"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["192.168.1.1/32"] # Change this to the real address after provision
  }
  # allow all outbound
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
  # tags
  tags = {
    Name        = "Jenkins"
    Description = "A secutiry group for the Jenkins server"
  }
}

data "template_file" "user_data" {
  template = file("jenkins.sh")
}

resource "aws_instance" "jenkins" {
  ami                         = data.aws_ami.ubuntu.id
  instance_type               = "t3.medium"
  subnet_id                   = aws_subnet.public.id
  vpc_security_group_ids      = [aws_security_group.jenkins.id]
  associate_public_ip_address = true
  key_name                    = "frankfurt"
  user_data                   = file("jenkins.sh")
  tags = {
    Name = "Jenkins"
  }
}
